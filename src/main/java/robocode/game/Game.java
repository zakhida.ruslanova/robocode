package robocode.game;

import robocode.gamearena.Arena;
import robocode.model.Optimus;
import robocode.robotsdetails.Position;

import java.util.ArrayList;
import java.util.List;

public class Game {

    private Arena arena;
    private List<Optimus> robots;

    public Game() {
        this.arena = new Arena(500,500);
        this.robots = new ArrayList<>();
    }

    public static void main(String[] args) {
        Game game = new Game();
        Optimus optimus = new Optimus(1, "Optimus", true, 100, new Position(130, 250));
        game.addRobotsToTheGame(optimus);
    }

    private void addRobotsToTheGame(Optimus optimus) {
        System.out.println("Robot: "  + optimus.getName() + " is added to the game "  + " on position: " + optimus.getPosition().toString());
        robots.add(optimus);
    }

}
