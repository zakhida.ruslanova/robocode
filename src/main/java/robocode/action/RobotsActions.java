package robocode.action;

public interface RobotsActions {
    void run();
    void shoot();
    void recreation();
    void searchEnemy();
}